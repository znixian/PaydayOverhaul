--[[
    Initializer
   
   Set up all the utilities we'll use later on, such as our version of dofile.
]]

local REALM_ID = "xyz.znix.mod.Overhaul" -- Also used in Core.lua

local R = {}
R.BasePath = ModPath
R.REALM_ID = REALM_ID
_G[REALM_ID] = R

-- Utilities
function R.loadfile(filename, scope)
	local filename_full = R.BasePath .. filename
	local func, err
	if vm and type(vm.loadfile) == "function" then
		-- using BLT4L, we have the VM table
		func, err = vm.loadfile(filename_full)
	else
		local f = io.open(filename_full, "rb")
		local content = f:read("*all")
		f:close()
		
		func, err = loadstring(content, filename_full)
	end
	
	if err then
		R.error("ERR_LOADING: " .. err)
	end
	
	scope = scope or R.env
	setfenv(func, scope)
	return func
end

function R.dofile(filename, ...)
	return R.loadfile(filename, nil)(...)
end

function R.error(message, level)
	error("[Overhaul] " .. message, (level or 1) + 1)
end

function R.print(message, ...)
	for _, msg in ipairs({...}) do
		message = message .. " " .. tostring(msg)
	end
	log("[Overhaul] " .. message)
end

function R.define(name)
	if R.env[name] then
		R.error("Cannot redefine " .. name)
	end
	
	rawset(R.env, name, {})
end

-- Set up the custom environment
local env = {
	__G = _G,
	_R = R,

	loadfile = R.loadfile,
	dofile = R.dofile,
	error = R.error,
	print = R.print
}
env._G = env
R.env = env

setmetatable(env, {
	__index = _G,
	__newindex = function(_, name, val)
		R.error("cannot set env index " .. name .. " to " .. tostring(val), 2)
	end
})

-- Set up the rest of the mod
R.loadfile("savefile/SaveFile.lua")()

-- Our system for setting up hooks
-- WARNING: Make sure you add matching entries in mod.txt
R._hooks = {
-- 	["lib/managers/achievmentmanager"] = "savefile/AchievmentManager.lua"
--	["lib/managers/savefilemanager"] = "savefile/SaveFileManager.lua"
	["lib/managers/systemmenumanager"] = "savefile/SystemMenuManager.lua",
	
	["lib/tweak_data/charactertweakdata"] = "balance/CharacterTweakData.lua",
	["lib/tweak_data/skilltreetweakdata"] = "balance/SkillTreeTweakData.lua",
	["lib/tweak_data/upgradestweakdata"] = "balance/UpgradesTweakData.lua",
	["lib/tweak_data/weapontweakdata"] = "balance/WeaponTweakData.lua",
	["lib/tweak_data/dlctweakdata"] = "balance/DLCTweakData.lua",
	["lib/network/matchmaking/networkmatchmakingsteam"] = "net/NetworkMatchmakingSteam.lua"
}

-- And load their files
for name, path in pairs(R._hooks) do
	R._hooks[name] = R.loadfile(path)
	
	-- local classname = name:match("^.+/([^/]).lua$")
	-- rawset(env, classname, {})
end
