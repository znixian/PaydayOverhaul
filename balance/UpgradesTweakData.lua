--[[
    Upgrades Tweak Data
   
   Changes the numerical values on skills and perk decks for
   balance reasons.
   
   For changes to the actual skill tree, see SkillTreeTweakData
]]

local function append(name, func)
	local old = UpgradesTweakData[name]
	if not old then error("Cannot override " .. name .. " for UpgradesTweakData") end
	UpgradesTweakData[name] = function(...)
		local val = old(...)
		return func(...) or val
	end
end

append("init", function(self, tweak_data)
	-- Remove removed weapons from level_tree
	
	for level, data in pairs(self.level_tree) do
		if data.upgrades then
			local new_upgrades = {}
			
			for _, id in ipairs(data.upgrades) do
				local upgrade = self.definitions[id]
				if upgrade.category ~= "weapon" or WeaponTweakDataModifier:Included(id) then
					table.insert(new_upgrades, id)
				else
					print("Removed", id, "from level", level)
				end
			end
			
			data.upgrades = new_upgrades
		end
	end
end)

local function setup_cupcakes(self, skills)
	local qty1, qty2 = 2, 4
	
	-- Cut down on the number of first aid kits
	self.values.first_aid_kit.quantity = {
		qty1,
		qty1 + qty2
	}
	
	skills.tea_cookies = { -- More first aid kits
		{
				qty2,
				qty1
		},
		{
				aty1,
				"5",
				qty2,
				"20"
		}
	}
end

local function setup_hostagetaker(self, skills)
	local hp1 = 0.5
	local hp2 = 2.0
	
	self.values.player.hostage_health_regen_addend = {
		hp1 / 100,
		hp2 / 100
	}
	
	skills.black_marketeer = {
		{
			tostring(hp1) .. "%",
			"5"
		},
		{
			tostring(hp2) .. "%",
			"5"
		}
	}
end

local function setup_muscle(self, perks) -- Nerf Muscle
	local regen = 1 -- 1% per 5 seconds
	
	self.values.player.passive_health_regen[1] = regen / 100
	perks[2][9].multiperk2 = tostring(regen) .. "%"
end

local function setup_gambler(self, perks) -- Nerf Gambler
	local hp_min = 4
	local hp_max = 6
	local hp_bonus = 2
	
	-- IDK if this works properly
	self.loose_ammo_restore_health_values.multiplier = 0.2 / 4 -- 0.2 by default
	
	perks[10][1].multiperk = tostring(hp_min)
	perks[10][1].multiperk2 = tostring(hp_max)
	
	perks[10][7].multiperk = tostring(hp_bonus)
	perks[10][9].multiperk = tostring(hp_bonus)
end

append("_init_pd2_values", function(self)
	-- Copy values to the UI part
	local editable_skill_descs = {}
	
	setup_cupcakes			(self, editable_skill_descs)
	setup_hostagetaker		(self, editable_skill_descs)
	
	for skill_id, skill_desc in pairs(editable_skill_descs) do
		for index, skill_version in ipairs(skill_desc) do
			local version = index == 1 and "multibasic" or "multipro"

			for i, desc in ipairs(skill_version) do
				self.skill_descs[skill_id][version .. (i == 1 and "" or tostring(i))] = tostring(desc)
			end
		end
	end
	
	-- Modify the perk decks
	
	-- Remove the ones we don't like
	local perks = self.specialization_descs -- Grab the current set of perk deck values
	self.specialization_descs = {} -- Clear out the perk deck's values
	SkillTreeTweakDataModifier:ModifySpecializationData(self.specialization_descs, perks) -- Copy across the desired ones
	
	setup_muscle(self, perks)
	setup_gambler(self, perks)
	
end)
