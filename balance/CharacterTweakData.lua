--[[
    Character Tweak Data
   
   Tweak data for cops and possibly players.
   
   Use our own system for determining how much health and damage they deal/take
]]

-- TODO set charactor speeds, same as difficulty

-- Null out any previous HP changes
local old_mult_hp = CharacterTweakData._multiply_all_hp
function CharacterTweakData:_multiply_all_hp(hp_mul, hs_mul) end

local difficulties = {
	easy = {
		health_mult = 1,
		headshot_mult = 1,
	},
	normal = {
		health_mult = 1,
		headshot_mult = 1,
	},
	hard = {
		health_mult = 1.5,
		headshot_mult = 1,
	},
	overkill = { -- Very Hard
		health_mult = 1.5,
		headshot_mult = 1,
	},
	overkill_145 = { -- Overkill
		health_mult = 2,
		headshot_mult = 1,
	},
	easy_wish = { -- Mayhem
		health_mult = 2,
		headshot_mult = 1,
	},
	overkill_290 = { -- Death Wish
		health_mult = 4,
		headshot_mult = 2,
	},
	sm_wish = { -- One Down
		health_mult = 4,
		headshot_mult = 2,
	}
}

for name, data in pairs(difficulties) do
	local setup_func = CharacterTweakData["_set_" .. name]
	
	-- Make sure the setup function is valid
	if not setup_func then error("Missing setup func for " .. name) end
	
	-- Add our stuff on
	CharacterTweakData["_set_" .. name] = function(self, ...)
		setup_func(self, ...)
		
		old_mult_hp(self, data.health_mult, data.headshot_mult)
	end
end
