
local old_init = DLCTweakData.init
function DLCTweakData:init(tweak_data, ...)
	old_init(self, tweak_data, ...)
	
	-- Remove removed weapons from upgrades
	for dlc_name, dlc_data in pairs(self) do
		if dlc_data.content then
			local data = dlc_data.content
			if data.upgrades then
				local new_upgrades = {}
				
				for _, id in ipairs(data.upgrades) do
					local upgrade = tweak_data.upgrades.definitions[id]
					if upgrade.category ~= "weapon" or WeaponTweakDataModifier:Included(id) then
						table.insert(new_upgrades, id)
					else
						print("Removed", id, "from level", level)
					end
				end
				
				data.upgrades = new_upgrades
			end
		end
	end
end
