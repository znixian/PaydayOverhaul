
_R.define("WeaponTweakDataModifier")

local allowed_weapons = {
	-- Default Weapons
	"amcar",
	"glock_17",
	
	-- NPC Weapons
	"swat_van_turret_module",
	"ceiling_turret_module",
	"m4_crew",
	
	-- Misc Allowed Weapons
	"uzi",
	"tec9",
	"akmsu",
	"ppk",
	"g36",
	"saw",
	"benelli",
	"x_deagle",
	"msr",
	"scar",
	"glock_18c",
	"olympic",
	"siltstone",
	"mp7",
	"baka",
	"asval",
	"flamethrower_mk2",
	"gre_m79",
	"pl14",
	"s552",
	"rpg7",
	"m134",
	"mosin",
	"fal",
	"m95",
	"ak74",
	"judge",
	"deagle",
	"colt_1911",
	"x_akmsu",
	"akm",
	"spas12",
	"ksg",
	"serbu"
}
	
local weaponMap = {}
for _, name in ipairs(allowed_weapons) do
	weaponMap[name] = true
end

function WeaponTweakDataModifier:Included(name)
	return weaponMap[name] or string.sub(name, -4)=="_npc"
end

local old_init = WeaponTweakData.init
function WeaponTweakData:init(...)
	old_init(self, ...)
	
	-- Remove the weapons we don't like
	for name, def in pairs(self) do
		if def.DAMAGE then
			if not WeaponTweakDataModifier:Included(name) then
				self[name] = nil
			end
		end
	end
end
