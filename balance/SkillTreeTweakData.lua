--[[
    Skilltree Tweak Data
   
   Modifies/rebalances parts of the skill trees and specializations (perk decks).
   
   For numerical changes, please see UpgradesTweakData
]]

_R.define("SkillTreeTweakDataModifier")

-- If you have a list of data containing one entry for
-- each perk deck in the game, this function can transform it
-- to be correct when used with this mod.
--
-- It copies values from `from` into `target` when their indexes
-- match one of the perk decks we want to keep in the game.
--
-- Please try to avoid indexing `target` with hardcoded values,
-- as even the order of the resultant perk decks may change in future
-- versions of this mod.
function SkillTreeTweakDataModifier:ModifySpecializationData(target, from)
	for _, id in ipairs({
		1, -- Crew Chief
		2, -- Muscle
		3, -- Armorer
		4, -- Rogue
		5, -- Hitman
		
		7, -- Burglar
		10, -- Gambler
	}) do
		table.insert(target, from[id])
	end
end

-- Get a skill tree column based on it's page and position
local function getTree(self, page, num)
	local tree = self.trees[(page-1)*3 + num]
	
	if not tree then
		error("Missing tree at pg" .. tostring(page) .. ", col" .. tostring(num))
	end
	
	return tree
end

local old_init = SkillTreeTweakData.init
function SkillTreeTweakData:init(...)
	old_init(self, ...)
	
	-- Mastermind pg1, tree2: Swap Stockholm Syndrome and Confident (2x jokers)
	getTree(self, 1, 2).tiers = {
		{"triathlete"},
		{
			"stockholm_syndrome",
			"joker"
		},
		{
			"cable_guy", -- Confident
			"control_freak"
		},
		{"black_marketeer"}
	}
	
	
	-- Remove perk decks we don't like
	
	local orig_perks = self.specializations -- Grab the current set of perks
	self.specializations = {} -- Clear out the perk decks and their values
	SkillTreeTweakDataModifier:ModifySpecializationData(self.specializations, orig_perks) -- Copy across the desired ones
	
	-- Remove the unlock-by-default for akimbo weapons
	for _, name in ipairs({
		"jowi",
		"x_1911",
		"x_b92fs",
		"x_deagle",
		"x_g22c",
		"x_g17",
		"x_usp",
		"x_sr2",
		"x_mp5",
		"x_akmsu",
		"x_packrat"
	}) do
		local target_index
		for index, tname in pairs(self.default_upgrades) do
			if name == tname then
				target_index = index
			end
		end
		
		if target_index ~= nil then
			table.remove(self.default_upgrades, target_index)
		end
	end
end
