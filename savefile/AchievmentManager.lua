--[[
    AchievmentManager
   Override the default achievement system to use SaveFile
   For safety reasons (in case any extra functions are introduced), make sure
   we override the entire class. This might result in crashes later on, but
   it's better than cross-contamination between the two instances.
]]--
print("Loaded achievement system")

__G.AchievmentManager = class() -- Overwrite what was there before
AchievmentManager.PATH = "gamedata/achievments"
AchievmentManager.FILE_EXTENSION = "achievment"
function AchievmentManager:init()
	self.exp_awards = {
		none = 0,
		a = 500,
		b = 1500,
		c = 5000
	}
	self.script_data = {}
	
	self:_parse_achievments("Steam")
	self:fetch_achievments()
	
	self.handler = {}
	function self.handler:has_achievement(id)
		return false --HasAchievment(id)
	end
	
	Global.achievment_manager = {
		achievments = self.achievments
	}
	
	self._mission_end_achievements = {}
end
function AchievmentManager:init_finalize() end
function AchievmentManager:fetch_achievments()
	for id, ach in pairs(self.achievments) do
		if HasAchievment(ach.id) then
			ach.awarded = true
		end
	end
end
function AchievmentManager:_parse_achievments(platform)
	local list = PackageManager:script_data(self.FILE_EXTENSION:id(), self.PATH:id())
	self.achievments = {}
	for _, ach in ipairs(list) do
		if ach._meta == "achievment" then
			for _, reward in ipairs(ach) do
				if reward._meta == "reward" and platform == reward.platform then
					self.achievments[ach.id] = {
						id = reward.id,
						name = ach.name,
						exp = self.exp_awards[ach.awards_exp],
						awarded = false,
						dlc_loot = reward.dlc_loot or false
					}
				end
			end
		end
	end
end
function AchievmentManager:get_script_data(id)
	return self.script_data[id]
end
function AchievmentManager:set_script_data(id, data)
	self.script_data[id] = data
end
function AchievmentManager:exists(id)
	return self.achievments[id] ~= nil
end
function AchievmentManager:get_info(id)
	return self.achievments[id]
end
function AchievmentManager:total_amount()
	return table.size(self.achievments)
end
function AchievmentManager:total_unlocked()
	local i = 0
	for _, ach in pairs(self.achievments) do
		if ach.awarded then
			i = i + 1
		end
	end
	return i
end
function AchievmentManager:add_heist_success_award(id)
	self._mission_end_achievements[id] = {award = true}
end
function AchievmentManager:add_heist_success_award_progress(id)
	local new_progress = (managers.job:get_memory(id, true) or 0) + 1
	managers.job:set_memory(id, new_progress, true)
	self._mission_end_achievements[id] = {stat = true, progress = new_progress}
end
function AchievmentManager:clear_heist_success_awards()
	self._mission_end_achievements = {}
end
function AchievmentManager:heist_success_awards()
	return self._mission_end_achievements
end
function AchievmentManager:award(id)
	if not self:exists(id) then
		Application:error("[AchievmentManager:award] Awarding non-existing achievement", "id", id)
		return
	end
	managers.challenge:on_achievement_awarded(id)
	managers.custom_safehouse:on_achievement_awarded(id)
	if managers.mutators:are_achievements_disabled() then
		return
	end
	if self:get_info(id).awarded then
		return
	end
	managers.mission:call_global_event(Message.OnAchievement, id)
	
	-- save achievments
	SetAchievment(id, true)
	SaveProgress()
	
	-- Give the reward items
	print("[AchievmentManager:award] ", id)
	local data = self:get_info(id)
	data.awarded = true
	if data.dlc_loot then
		managers.dlc:on_achievement_award_loot()
	end
end
function AchievmentManager:award_progress(stat, value)
	managers.challenge:on_achievement_progressed(stat)
	managers.custom_safehouse:on_achievement_progressed(stat, value)
	if managers.mutators:are_mutators_active() and game_state_machine:current_state_name() ~= "menu_main" then
		return
	end
	
	value = value or 1
	
	progress = GetProgress(stat) or 0
	
	progress = progress + value
	
	SetProgress(stat, progress)
	
	-- TODO how do we know when to save?
end
function AchievmentManager:get_stat(stat)
	return GetProgress(stat)
end
function AchievmentManager:check_complete_heist_stats_achivements()
	local job
	for achievement, achievement_data in pairs(tweak_data.achievement.complete_heist_stats_achievements) do
		local available_jobs
		if achievement_data.contact == "all" then
			available_jobs = {}
			for _, list in pairs(tweak_data.achievement.job_list) do
				for _, job in pairs(list) do
					table.insert(available_jobs, job)
				end
			end
		else
			available_jobs = deep_clone(tweak_data.achievement.job_list[achievement_data.contact])
		end
		for id = #available_jobs, 1, -1 do
			job = available_jobs[id]
			if type(job) == "table" then
				for _, job_id in ipairs(job) do
					local break_outer = false
					for _, difficulty in ipairs(achievement_data.difficulty) do
						if managers.statistics:completed_job(job_id, difficulty) > 0 then
							table.remove(available_jobs, id)
							break_outer = true
						else
						end
					end
					if break_outer then
					else
					end
				end
			else
				for _, difficulty in ipairs(achievement_data.difficulty) do
					if managers.statistics:completed_job(job, difficulty) > 0 then
						table.remove(available_jobs, id)
					end
				end
			end
		end
		if table.size(available_jobs) == 0 then
			self:_award_achievement(achievement_data)
		end
	end
end
function AchievmentManager:check_autounlock_achievements()
	self:_check_autounlock_complete_heist()
	self:_check_autounlock_difficulties()
	self:_check_autounlock_infamy()
end
function AchievmentManager:_check_autounlock_complete_heist()
	for achievement, achievement_data in pairs(tweak_data.achievement.complete_heist_achievements) do
		if table.size(achievement_data) == 3 and achievement_data.award and achievement_data.difficulty and (achievement_data.job or achievement_data.jobs) then
			
			local jobs = achievement_data.jobs
			if not jobs then
				jobs = {
					achievement_data.job
				}
			end
			for i, job in pairs(jobs) do
				for _, difficulty in ipairs(achievement_data.difficulty) do
					if managers.statistics:completed_job(job, difficulty) > 0 then
						self:_award_achievement(achievement_data)
					else
					end
				end
			end
		end
	end
end
function AchievmentManager:_check_autounlock_difficulties()
	self:check_complete_heist_stats_achivements()
end
function AchievmentManager:_check_autounlock_infamy()
	managers.experience:_check_achievements()
end
function AchievmentManager:_award_achievement(achievement_data, achievement_name)
	if achievement_name then
		print("[AchievmentManager] awarding: ", achievement_name)
	end
	if achievement_data.stat then
		managers.achievment:award_progress(achievement_data.stat)
	elseif achievement_data.award then
		managers.achievment:award(achievement_data.award)
	elseif achievement_data.challenge_stat then
		managers.challenge:award_progress(achievement_data.challenge_stat)
	elseif achievement_data.trophy_stat then
		managers.custom_safehouse:award(achievement_data.trophy_stat)
	elseif achievement_data.challenge_award then
		managers.challenge:award(achievement_data.challenge_award)
	end
end
