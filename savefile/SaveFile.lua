
_R.define("SaveFile")

local BINSER = loadfile("binser.lua")()

local SAVE_NAME = os.getenv("PD2_SAVE_NAME") or "overhaul_progression.dat"
local SAVE_LOCATION = SavePath .. SAVE_NAME

local SAVEGAME_DATA = {}

function SaveFile:HasAchievment(id)
	local res = SAVEGAME_DATA.achievments[id] and true or false
	return res
end

function SaveFile:SetAchievment(id, state)
	SAVEGAME_DATA.achievments[id] = state
end

function SaveFile:GetProgress(id)
	return SAVEGAME_DATA.stats[id]
end

function SaveFile:SetProgress(id, state)
	SAVEGAME_DATA.stats[id] = state
end

function SaveFile:LoadProgress()
	print("Reading Progress File")
	
	local result, len = nil, 0
	if io.file_is_readable(SAVE_LOCATION) then
		result, len = BINSER.readFile(SAVE_LOCATION)
	end
	
	if len < 1 then
		SAVEGAME_DATA = {}
	else
		SAVEGAME_DATA = result[1]
	end
	
	SAVEGAME_DATA.achievments = SAVEGAME_DATA.achievments or {}
	SAVEGAME_DATA.stats = SAVEGAME_DATA.stats or {}
	
	print("done Reading Progress File")
end

function SaveFile:SaveProgress()
	print("Saving Progress File")
	
	BINSER.writeFile(SAVE_LOCATION, SAVEGAME_DATA)
	
-- 	file = io.open("dump-ugly.json", "w")
-- 	file:write(json.encode(SAVEGAME_DATA))
-- 	file:close()
end

---------------------------
----  Callback manager ----
-- Due to some... interesting quirks, PD2 has something of
-- a race condition - it first starts loading the savefile data
-- as part of CoreSetup:init_managers, however the game will
-- crash if that data is ready before the GUI is set up in
-- CoreSetup:init_finalize (although this may only be the first time
-- the profile is opened, haven't tested otherwise yet).
--
-- As we normally call our callbacks instantly, this poses a bit
-- of a problem. Therefore, callbacks are queued up until SystemMenuManager
-- tells us.
---------------------------

local callback_queue = {}
local function run_cb(callback, ...)
	if callback_queue then
		table.insert(callback_queue, {func = callback, args={...} })
	else
		callback(...)
	end
end

function SaveFile:MenusReady()
	if not callback_queue then return end
	
	for _, cb in ipairs(callback_queue) do
		cb.func( unpack(cb.args) )
	end
	
	callback_queue = nil
end

---------------------------
----  SaveGameManager  ----
---------------------------

-- Defined in C++, so overwrite it as soon as we can (don't have to wait for it to be loaded).
__G.SaveGameManager = {}

function SaveGameManager:set_max_nr_slots(slots)
	print("slots", slots)
end

function SaveGameManager:iterate_savegame_slots(task, callback)
	local result = {}
	
	for i=task.first_slot,task.last_slot do
		result[i] = SAVEGAME_DATA[i]
	end
	
	print("iterate_savegame_slots", #result, task.first_slot, task.last_slot)
	
	run_cb(callback, task, result)
end

function SaveGameManager:save(task, callback)
	local slot = task.first_slot
	local data = task.data[1]
	
-- 	print("save", slot, data)
	
	SAVEGAME_DATA[slot] = data
	
	SaveFile:SaveProgress()
	
	print("Saving task", slot, task)
	run_cb(callback, task, {
		[slot] = {
			status="OK"
		}
	})
end

function SaveGameManager:load(task, callback)
	local slot = task.first_slot
	local data = SAVEGAME_DATA[slot]
	
	local slot_data = {}
	
	slot_data.data = data
	if data ~= nil then
		slot_data.status = "OK"
	end
	print("loding", slot, data)
	
	-- Make sure it's accepted.
	if data then
		data.user_id = SaveFile._USER_ID_OVERRRIDE or Steam:userid()
	end
	
	local result = {
		[slot] = slot_data
	}
	
-- 	print("load", #result, slot, result[slot])
	
	run_cb(callback, task, result)
end

function SaveGameManager:remove(task, callback)
	local slot = task.first_slot
	SAVEGAME_DATA[slot] = nil
	
	SaveFile:SaveProgress()
	
	run_cb(callback, task, {})
end

-- Log anything else being called
for _, name in ipairs({
	"save_extension",
	"save_path",
	"set_verbose",
	"set_save_extension",
	"type_id",
	"script_reference",
	"script_value",
	"extension",
	"set_save_path",
	"set_directory_name",
	"set_save_prefix",
	"directory_name",
	"save_prefix",
	"set_extension",
	"type_name",
	"alive",
	"abort",
	"key"
}) do
	SaveGameManager[name] = function(...)
		PrintTable({...})
		error("Function " .. name .. " called with args " .. tostring(...))
	end
end

-- Load the progress file
SaveFile:LoadProgress()
