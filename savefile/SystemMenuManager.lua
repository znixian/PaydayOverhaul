core:module("SystemMenuManager")

local old = GenericSystemMenuManager.init_finalize
function GenericSystemMenuManager:init_finalize(...)
	old(self, ...)
	
	_G["xyz.znix.mod.Overhaul"].env.SaveFile:MenusReady()
	
-- 	for name, def in pairs(_G.tweak_data.upgrades.definitions) do
-- 		if def.category == "weapon" then
-- 			_G["xyz.znix.mod.Overhaul"].env.print("Weapon " .. name .. "----" .. managers.localization:text("bm_w_" .. name))
-- 		end
-- 	end
end
