
local REALM_ID = "xyz.znix.mod.Overhaul" -- Also used in Init.lua

if not _G[REALM_ID] then
	dofile(ModPath .. "Init.lua")
end

local R = _G[REALM_ID]

if R._hooks[RequiredScript] then
	R._hooks[RequiredScript]()
end
