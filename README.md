# PD2 Overhaul Mod
A balance overhaul mod for PAYDAY 2, greatly reducing enemy health, disabling
or nerfing most sources of player healing, and disabling many weapons.

# Saves
Currently, this replaces the PD2 saving system, allowing for alternative
saves.

When the custom save system is enabled, the game will be saved to
```
<PD2 dir>/mods/saves/overhaul_progression.dat
```

# License

This software is licensed under the GNU GPL, version 3.
The full text of this license can be found in the file
named `LICENSE`

The [binser](https://github.com/bakpakin/binser) library, found in `binser.lua`, is licensed
under the BSD License. If you haven't read this license before,
you can find a copy in `LICENSE_BINSER`

